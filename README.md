This is documentation for an energy monitoring device built around the nRF52832 system-on-chip from Nordic.  It includes a 2.4 GHz BLE-enabled radio transceiver, a Cortex M4F microcontroller, and a host of hardware peripherals.  The device is capable of extracting current and voltage waveforms from a standard wall power plug without contacting the mains power.  The current is measured inductively, as a mild steel flux guide channels magnetic flux through a sensitive hall effect device.  The voltage is measured capacitively, as the flux guides couple with the conductors in the plug.  Measuring both quantities allows us to separate active from apparent power, measuring the real power used by an appliance plugged through our device.

<a href='bc832-watt'>bc832-watt/</a> includes source files for a PCB built around the BC-832 module (containing the nRF52) from Fanstel.

<img src='img/bc832-watt-1.jpg' width=40%>
<img src='img/bc832-watt-3.jpg' width=44%>

This device uses the <a href='http://www.ti.com/lit/ds/symlink/drv5053.pdf'>DRV5053VA</a> hall effect sensor to produce around 0.1 volts/amp out of the sensor (or ~1 mV per Watt through the plug).  The nrf adc has a +-0.6V reference, 4x gain, and 12-bit conversion, which might resolve 100mW (maybe finer with oversampling).  Certainly not precision measurement, but good enough for monitoring appliances.

Actually, in this first design, I forgot that the hall effect sensor has a 1V output baseline (when field is zero).  This means in this design can't use the ADC gain in the BC832.  To fix this, I should use a sensors near each conductor and measure differentially between them.  Not only will this eliminate the 1V measurement offset, but it should double the signal-to-noise ratio.

 The nRF52 radio+microcontroller draws 5mA in transmit and receive, so it could operate well within the specs of a coin cell, or harvest from the power line (say, using http://www.linear.com/product/LTC3588-1) to charge a capacitor for power.  
 
Below, we see the flux guides registered into pockets on the pcb. 

<img src='img/bc832-watt-0.jpg' width=40%>



Notes from previous versions: http://vulpes.cba.mit.edu/rnd/watt/


### Power requirements

The nRF52832 draws 5mA peak @ 3.3V

The Hall Effect Sensor draws xmA @ 3.3V

Assume whole device runs at peak load for 5% of the time
```math 
P = IV = 5 * 10^{-3} * 3.3 * 0.05 = 0.825 mW 
```
```math 
1 Wh = 1~watt~for~1~hour
```
```math
0.825~mW~for~365~days = 7.2 Wh
```
```math
E [Wh] = \frac{Q [mAh] * V [V]}{1000}
```
```math
Q = \frac{1000 * 7.2}{3.3} = 2181 mAh
```

The CR2477 is one of the largest coin cell batterys (20mm diameter and 4.7mm thickness) and has a capacity of ~1000 mAh. 

An Alkaline AA battery can supply 1.5V with a capacity of 1800–2600 mAh


 

