#define N_SAMPLES 48
#define PACKET_BASE_ADDRESS_LENGTH  (2UL)  //Packet base address length field size in bytes

//static uint8_t packet = 52; //declare a buffer for our packet to use with easydma

const uint8_t pin_rx = 8;
const uint8_t pin_tx = 6;
uint16_t last_tx_time = 0;
uint16_t tx_period = 100; //ms

static int16_t data_buffer[N_SAMPLES] = {0};

//
//RADIO
//
void radio_setup(){
  NRF_RADIO->POWER = RADIO_POWER_POWER_Disabled; //turn off radio to reset registers
  delay(10);
  NRF_RADIO->POWER = RADIO_POWER_POWER_Enabled; //turn on radio
  delay(10);

  NRF_RADIO->TXPOWER   = (RADIO_TXPOWER_TXPOWER_Pos3dBm << RADIO_TXPOWER_TXPOWER_Pos);
  NRF_RADIO->FREQUENCY = 7UL;  // Frequency bin 7, 2407MHz
  NRF_RADIO->MODE      = (RADIO_MODE_MODE_Nrf_2Mbit << RADIO_MODE_MODE_Pos);

  NRF_RADIO->PREFIX0 = ((uint32_t)0xC0 << 0); // Prefix byte of address 0
  NRF_RADIO->BASE0 = 0x01234567UL;  // Base address for prefix 0
  NRF_RADIO->TXADDRESS   = 0x00UL;  // Set device address 0 to use when transmitting
  NRF_RADIO->RXADDRESSES = 0x01UL;  // Enable device address 0 to use to select which addresses to receive  

  // Packet configuration
  NRF_RADIO->PCNF0 = (0 << RADIO_PCNF0_S1LEN_Pos) | (0 << RADIO_PCNF0_S0LEN_Pos) | (0 << RADIO_PCNF0_LFLEN_Pos); 
  NRF_RADIO->PCNF1 = (RADIO_PCNF1_WHITEEN_Disabled << RADIO_PCNF1_WHITEEN_Pos) |
                     ((2*N_SAMPLES) << RADIO_PCNF1_STATLEN_Pos) |
                     (RADIO_PCNF1_ENDIAN_Big       << RADIO_PCNF1_ENDIAN_Pos)  |
                     (2 << RADIO_PCNF1_BALEN_Pos);
  NRF_RADIO->CRCCNF = (RADIO_CRCCNF_LEN_Disabled << RADIO_CRCCNF_LEN_Pos); // Number of checksum bits
  NRF_RADIO->MODECNF0 |= RADIO_MODECNF0_RU_Fast << RADIO_MODECNF0_RU_Pos; //turn on fast ramp up
  NRF_RADIO->SHORTS = 0; //turn off all shortcuts, for debug
  NRF_RADIO->PACKETPTR = (uint32_t)&data_buffer; //set pointer to packet buffer
  //start HFCLK
  NRF_CLOCK->TASKS_HFCLKSTART = 1;
  while(!(NRF_CLOCK->HFCLKSTAT & CLOCK_HFCLKSTAT_STATE_Msk)); //wait for hfclk to start
  delay(10);
}

void radio_wait_for_end(){ while(!(NRF_RADIO->EVENTS_END)); NRF_RADIO->EVENTS_END = 0; } //clear end event
void radio_wait_for_ready(){ while(!(NRF_RADIO->EVENTS_READY)); NRF_RADIO->EVENTS_READY = 0; } //clear ready event
void radio_disable(){
  NRF_RADIO->EVENTS_DISABLED = 0; //clear disabled event
  NRF_RADIO->TASKS_DISABLE = 1;
  while(!(NRF_RADIO->EVENTS_DISABLED)); 
}
void radio_send(){
  NRF_RADIO->EVENTS_READY = 0; //clear ready event
  NRF_RADIO->TASKS_TXEN=1; //trigger tx enable task
  delayMicroseconds(15);
  //radio_wait_for_ready(); //only generated when actually switching to tx mode
  NRF_RADIO->TASKS_START=1;  //start
  radio_wait_for_end();
}
void radio_recv(){
  NRF_RADIO->EVENTS_READY = 0; //clear ready event
  NRF_RADIO->TASKS_RXEN=1; //trigger rx enable task
  delayMicroseconds(15);
  //radio_wait_for_ready(); //only generated when actually switching to rx mode 
  NRF_RADIO->TASKS_START=1;
  radio_wait_for_end();
}

//
//UARTE
//
void uarte_setup(){
  //uart with dma
  NRF_UARTE0->PSEL.TXD = (pin_tx << UARTE_PSEL_TXD_PIN_Pos) & UARTE_PSEL_TXD_PIN_Msk;
  NRF_UARTE0->PSEL.RXD = (pin_rx << UARTE_PSEL_RXD_PIN_Pos) & UARTE_PSEL_RXD_PIN_Msk;
  NRF_UARTE0->CONFIG =  ((UART_CONFIG_PARITY_Excluded << UARTE_CONFIG_PARITY_Pos) & UARTE_CONFIG_PARITY_Msk) 
                      | ((UARTE_CONFIG_HWFC_Disabled << UARTE_CONFIG_HWFC_Pos) & UARTE_CONFIG_HWFC_Msk);
  NRF_UARTE0->BAUDRATE = UART_BAUDRATE_BAUDRATE_Baud1M;
  NRF_UARTE0->ENABLE = (UARTE_ENABLE_ENABLE_Enabled << UARTE_ENABLE_ENABLE_Pos) & UARTE_ENABLE_ENABLE_Msk;
  NRF_UARTE0->TXD.MAXCNT = N_SAMPLES;
}

void setup() {
  uarte_setup();
  radio_setup();
  while(true){
    if (millis() - last_tx_time > tx_period){
      radio_recv();
      //start a transmission of the tx buffer
      NRF_UARTE0->TXD.PTR = (uint32_t)(&data_buffer);  //reset pointer to start of buffer
      NRF_UARTE0->TASKS_STARTTX = 1;  //trigger start task
      last_tx_time = millis();     
    }
  }
}

void loop() {}
