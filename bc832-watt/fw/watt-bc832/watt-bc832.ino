#define N_SAMPLES 48
#define PACKET_BASE_ADDRESS_LENGTH  (2UL)  //Packet base address length field size in bytes


const uint8_t pin_rx = 8;
const uint8_t pin_tx = 6;
uint16_t last_tx_time = 0;
uint16_t tx_period = 100; //ms

static int16_t data_buffer[N_SAMPLES] = {0};
static int16_t packet[N_SAMPLES] = {0}; //declare a buffer for our packet to use with easydma

//
//ADC
//
void adc_setup(){
  //configure SAADC resolution
  NRF_SAADC->RESOLUTION = SAADC_RESOLUTION_VAL_12bit;
  //configure sample rate via internal timer
  //NRF_SAADC->SAMPLERATE = (( SAADC_SAMPLERATE_MODE_Timers << SAADC_SAMPLERATE_MODE_Pos) & SAADC_SAMPLERATE_MODE_Msk) |
  //                        (( 2047 << SAADC_SAMPLERATE_CC_Pos) & SAADC_SAMPLERATE_CC_Msk);
  //enable oversampling -- do not combine with scan mode!
  //NRF_SAADC->OVERSAMPLE = (SAADC_OVERSAMPLE_OVERSAMPLE_Over256x << SAADC_OVERSAMPLE_OVERSAMPLE_Pos)  & SAADC_OVERSAMPLE_OVERSAMPLE_Msk ;
  NRF_SAADC->OVERSAMPLE = (SAADC_OVERSAMPLE_OVERSAMPLE_Bypass << SAADC_OVERSAMPLE_OVERSAMPLE_Pos)  & SAADC_OVERSAMPLE_OVERSAMPLE_Msk ;
  //enable SAADC
  NRF_SAADC->ENABLE = (SAADC_ENABLE_ENABLE_Enabled << SAADC_ENABLE_ENABLE_Pos);
  //set result pointer
  NRF_SAADC->RESULT.PTR = (uint32_t)(&data_buffer);
  NRF_SAADC->RESULT.MAXCNT = N_SAMPLES; // number of samples

  //set channel 0 resistor network, gain, reference, sample time, and mode
  NRF_SAADC->CH[0].CONFIG =   ((SAADC_CH_CONFIG_RESP_Bypass     << SAADC_CH_CONFIG_RESP_Pos)   & SAADC_CH_CONFIG_RESP_Msk)
                            | ((SAADC_CH_CONFIG_RESP_Bypass     << SAADC_CH_CONFIG_RESN_Pos)   & SAADC_CH_CONFIG_RESN_Msk)
                            | ((SAADC_CH_CONFIG_GAIN_Gain4      << SAADC_CH_CONFIG_GAIN_Pos)   & SAADC_CH_CONFIG_GAIN_Msk)
                            | ((SAADC_CH_CONFIG_REFSEL_Internal << SAADC_CH_CONFIG_REFSEL_Pos) & SAADC_CH_CONFIG_REFSEL_Msk)
                            | ((SAADC_CH_CONFIG_TACQ_5us        << SAADC_CH_CONFIG_TACQ_Pos)   & SAADC_CH_CONFIG_TACQ_Msk)
                            | ((SAADC_CH_CONFIG_BURST_Disabled  << SAADC_CH_CONFIG_BURST_Pos)   & SAADC_CH_CONFIG_BURST_Msk)
                            | ((SAADC_CH_CONFIG_MODE_Diff       << SAADC_CH_CONFIG_MODE_Pos)   & SAADC_CH_CONFIG_MODE_Msk);
  //configure Channel 0 to use A0 as positive, A1 as negative
  NRF_SAADC->CH[0].PSELP = SAADC_CH_PSELP_PSELP_AnalogInput0; 
  NRF_SAADC->CH[0].PSELN = SAADC_CH_PSELP_PSELP_AnalogInput1; 
  
  //set channel 1 resistor network, gain, reference, sample time, and mode
  NRF_SAADC->CH[1].CONFIG = ((SAADC_CH_CONFIG_RESP_Bypass       << SAADC_CH_CONFIG_RESP_Pos)   & SAADC_CH_CONFIG_RESP_Msk)
                            | ((SAADC_CH_CONFIG_RESP_Bypass     << SAADC_CH_CONFIG_RESN_Pos)   & SAADC_CH_CONFIG_RESN_Msk)
                            | ((SAADC_CH_CONFIG_GAIN_Gain4    << SAADC_CH_CONFIG_GAIN_Pos)   & SAADC_CH_CONFIG_GAIN_Msk)
                            | ((SAADC_CH_CONFIG_REFSEL_Internal << SAADC_CH_CONFIG_REFSEL_Pos) & SAADC_CH_CONFIG_REFSEL_Msk)
                            | ((SAADC_CH_CONFIG_TACQ_5us        << SAADC_CH_CONFIG_TACQ_Pos)   & SAADC_CH_CONFIG_TACQ_Msk)
                            | ((SAADC_CH_CONFIG_BURST_Disabled   << SAADC_CH_CONFIG_BURST_Pos)   & SAADC_CH_CONFIG_BURST_Msk)
                            | ((SAADC_CH_CONFIG_MODE_Diff         << SAADC_CH_CONFIG_MODE_Pos)   & SAADC_CH_CONFIG_MODE_Msk);
  //configure Channel 1 to use A2 as positive, A4 as negative
  NRF_SAADC->CH[1].PSELP = SAADC_CH_PSELP_PSELP_AnalogInput2; 
  NRF_SAADC->CH[1].PSELN = SAADC_CH_PSELP_PSELP_AnalogInput4; 
}

void take_adc_samples(){
  //start task
  NRF_SAADC->TASKS_START = 0x01UL;
  while (!NRF_SAADC->EVENTS_STARTED); NRF_SAADC->EVENTS_STARTED = 0x00UL;
  for(int i=0; i<N_SAMPLES; i++){
    //sample task channel 0
    NRF_SAADC->TASKS_SAMPLE = 0x01UL;
    while (!NRF_SAADC->EVENTS_DONE); NRF_SAADC->EVENTS_DONE = 0x00UL;
    //sample task channel 1
    NRF_SAADC->TASKS_SAMPLE = 0x01UL;
    while (!NRF_SAADC->EVENTS_DONE); NRF_SAADC->EVENTS_DONE = 0x00UL;
  } 
  NRF_SAADC->TASKS_STOP = 0x01UL;
  while (!NRF_SAADC->EVENTS_STOPPED); NRF_SAADC->EVENTS_STOPPED = 0x00UL;  
}

//
//RADIO
//
void radio_setup(){
  NRF_RADIO->POWER = RADIO_POWER_POWER_Disabled; //turn off radio to reset registers
  delay(10);
  NRF_RADIO->POWER = RADIO_POWER_POWER_Enabled; //turn on radio
  delay(10);

  NRF_RADIO->TXPOWER   = (RADIO_TXPOWER_TXPOWER_Pos3dBm << RADIO_TXPOWER_TXPOWER_Pos);
  NRF_RADIO->FREQUENCY = 7UL;  // Frequency bin 7, 2407MHz
  NRF_RADIO->MODE      = (RADIO_MODE_MODE_Nrf_2Mbit << RADIO_MODE_MODE_Pos);

  NRF_RADIO->PREFIX0 = ((uint32_t)0xC0 << 0); // Prefix byte of address 0
  NRF_RADIO->BASE0 = 0x01234567UL;  // Base address for prefix 0
  NRF_RADIO->TXADDRESS   = 0x00UL;  // Set device address 0 to use when transmitting
  NRF_RADIO->RXADDRESSES = 0x01UL;  // Enable device address 0 to use to select which addresses to receive  

  // Packet configuration
  NRF_RADIO->PCNF0 = (0 << RADIO_PCNF0_S1LEN_Pos) | (0 << RADIO_PCNF0_S0LEN_Pos) | (0 << RADIO_PCNF0_LFLEN_Pos); 
  NRF_RADIO->PCNF1 = (RADIO_PCNF1_WHITEEN_Disabled << RADIO_PCNF1_WHITEEN_Pos) |
                     ((2*N_SAMPLES) << RADIO_PCNF1_STATLEN_Pos) |
                     (RADIO_PCNF1_ENDIAN_Big       << RADIO_PCNF1_ENDIAN_Pos)  |
                     (2 << RADIO_PCNF1_BALEN_Pos);
  NRF_RADIO->CRCCNF = (RADIO_CRCCNF_LEN_Disabled << RADIO_CRCCNF_LEN_Pos); // Number of checksum bits
  NRF_RADIO->MODECNF0 |= RADIO_MODECNF0_RU_Fast << RADIO_MODECNF0_RU_Pos; //turn on fast ramp up
  NRF_RADIO->SHORTS = 0; //turn off all shortcuts, for debug
  NRF_RADIO->PACKETPTR = (uint32_t)&data_buffer; //set pointer to packet buffer
  //start HFCLK
  NRF_CLOCK->TASKS_HFCLKSTART = 1;
  while(!(NRF_CLOCK->HFCLKSTAT & CLOCK_HFCLKSTAT_STATE_Msk)); //wait for hfclk to start
  delay(10);
}

void radio_wait_for_end(){ while(!(NRF_RADIO->EVENTS_END)); NRF_RADIO->EVENTS_END = 0;} //clear end event  
void radio_wait_for_ready(){ while(!(NRF_RADIO->EVENTS_READY)); NRF_RADIO->EVENTS_READY = 0;} //clear ready event
void radio_disable(){
  NRF_RADIO->EVENTS_DISABLED = 0; //clear disabled event
  NRF_RADIO->TASKS_DISABLE = 1;
  while(!(NRF_RADIO->EVENTS_DISABLED)); 
}
void radio_send(){
  NRF_RADIO->EVENTS_READY = 0; //clear ready event
  NRF_RADIO->TASKS_TXEN=1; //trigger tx enable task
  delayMicroseconds(15);
  //radio_wait_for_ready(); //only generated when actually switching to tx mode
  NRF_RADIO->TASKS_START=1;  //start
  radio_wait_for_end();
}
void radio_recv(){
  NRF_RADIO->EVENTS_READY = 0; //clear ready event
  NRF_RADIO->TASKS_RXEN=1; //trigger rx enable task
  delayMicroseconds(15);
  //radio_wait_for_ready(); //only generated when actually switching to rx mode 
  NRF_RADIO->TASKS_START=1;
  radio_wait_for_end();
}

//
//UARTE
//
void uarte_setup(){
  //uart with dma
  NRF_UARTE0->PSEL.TXD = (pin_tx << UARTE_PSEL_TXD_PIN_Pos) & UARTE_PSEL_TXD_PIN_Msk;
  NRF_UARTE0->PSEL.RXD = (pin_rx << UARTE_PSEL_RXD_PIN_Pos) & UARTE_PSEL_RXD_PIN_Msk;
  NRF_UARTE0->CONFIG =  ((UART_CONFIG_PARITY_Excluded << UARTE_CONFIG_PARITY_Pos) & UARTE_CONFIG_PARITY_Msk) 
                      | ((UARTE_CONFIG_HWFC_Disabled << UARTE_CONFIG_HWFC_Pos) & UARTE_CONFIG_HWFC_Msk);
  NRF_UARTE0->BAUDRATE = UART_BAUDRATE_BAUDRATE_Baud1M;
  NRF_UARTE0->ENABLE = (UARTE_ENABLE_ENABLE_Enabled << UARTE_ENABLE_ENABLE_Pos) & UARTE_ENABLE_ENABLE_Msk;
  
  NRF_UARTE0->TXD.MAXCNT = N_SAMPLES;
}

void setup() {
  adc_setup();
  radio_setup();
  while(true){
    if (millis() - last_tx_time > tx_period){
      take_adc_samples();
      radio_send();
      last_tx_time = millis();   
    }
  }
}

void loop() {}
