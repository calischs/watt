#!/usr/bin/env python
import numpy as np
from math import *
import serial, time
import struct
import sys
import socket
import matplotlib.pyplot as plt
import matplotlib.animation as animation

n_samples = 24

def read_int16(bytes,signed=False):
	try:
		n = struct.unpack("<h",bytearray(bytes))[0] if signed else struct.unpack("<H",bytearray(bytes))[0]
		return n
	except:
		print "not properly formatted bytes: ",list(bytes)
		return None
def read(ser=None):
	if ser is None:
		print "No serial connection!"
	else:
		#line = bytearray()
		c = ser.read(4*n_samples)
		data = [read_int16(c[x:x+2],signed=True) for x in xrange(0,4*n_samples,2)]
		return data

def main():
	try:
		ser = serial.Serial(port='/dev/tty.usbserial-FTFMIM12',baudrate=1000000,timeout=1.)
		ser.isOpen()
		print "Established serial connection."
	except(OSError):
		#no serial port
		print "Couldn't find the serial port, entering debug mode."
		ser = None
	ser.flush()


	'''
	fig, ax = plt.subplots()
	line, = ax.plot(np.zeros(n_samples))
	ax.set_ylim([-1024,1024])
	def animate(i):
		try:
			data = read(ser=ser)
			print data[::2], data[1::2]
			line.set_ydata(data[::2])  # update the data
			return line,
		except(KeyboardInterrupt):
			return

	ani = animation.FuncAnimation(fig, animate, np.arange(1, 200),
                              interval=5, blit=False)
	plt.show()
	print "quitting"
	ser.close()
	sys.exit(0)
	'''



	while True: 
		#connection, address = serversocket.accept()

		try:
			data = read(ser=ser)
			print data[::2], data[1::2]
		except(KeyboardInterrupt):
			break
		#if data:
		#	clientsocket.send(data)
	print "quitting"
	ser.close()



if __name__ == '__main__':
	main()
